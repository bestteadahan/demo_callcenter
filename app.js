const express = require('express')
const session = require('express-session')  // 3rd-party middleware module
const cookieParser = require('cookie-parser')
const compression = require('compression')
const bodyParser = require('body-parser')  // Could parse body content from request
const morgan = require('morgan')

const path = require('path')
const fs = require('fs')

// HTTP method definition modules of each page
const index = require('./routes/index')
const login = require('./routes/login')
const sipRegister = require('./routes/controllers/sipRegister')
const logout = require('./routes/logout')
const forgetpw = require('./routes/forgetpw')
const resetpw = require('./routes/resetpw')
const hotline = require('./routes/hotline')
const saveLog = require('./routes/controllers/saveLog')
const accountinfo = require('./routes/accountinfo')
const historycalls = require('./routes/historycalls')
const callLog = require('./routes/callLog')
const accountmanage = require('./routes/accountmanage')

// Create express class
const app = express()

// Set options in app
app.set('views', 'views/')
app.set('view engine', 'pug')

// Use middlewares
// var accessLogStream = fs.createWriteStream(path.join(__dirname, 'history.log'), { flags: 'a' })
// app.use(morgan('dev', { stream: accessLogStream }))

app.use(cookieParser())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(express.static(__dirname + '/public'))
app.use(compression())

// Built session
app.use(session({
    secret: 'wattertek.com',  // 簽章sessionID的cookie
    resave: false,  // 強制每次req都存ression (即使沒修改)
    saveUninitialized: true
}))

// 設定第三方所需資訊
const conf = require('./config').config
app.locals.targetDB = conf.TARGETDB
app.locals.navtitle = conf.NAVTITLE

// Add modules into app
app.use('/', index)
app.use('/login', login)
app.use('/sipRegister', sipRegister)
app.use('/logout', logout)
app.use('/forgetpw', forgetpw)
app.use('/resetpw', resetpw)
app.use('/hotline', hotline)
app.use('/saveLog', saveLog)
app.use('/accountinfo', accountinfo)
app.use('/historycalls', historycalls)
app.use('/callLog', callLog)
app.use('/accountmanage', accountmanage)

app.listen(conf.PORTNUMBER, () => {
    console.log(`${conf.SERVICENAME}_Callcenter Service is Listening on port: ${conf.PORTNUMBER}`)
});