-- MySQL dump 10.13  Distrib 8.0.11, for Win64 (x86_64)
--
-- Host: ofw.cfrqis2mrcuf.ap-southeast-1.rds.amazonaws.com    Database: callcenter
-- ------------------------------------------------------
-- Server version	5.6.37-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `exts`
--

DROP TABLE IF EXISTS `exts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `exts` (
  `cid` varchar(36) NOT NULL,
  `available` tinyint(1) DEFAULT '1',
  `opid` int(11) DEFAULT NULL,
  `ext` varchar(4) NOT NULL,
  `lastUsed` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `history_calls`
--

DROP TABLE IF EXISTS `history_calls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `history_calls` (
  `callid` varchar(36) NOT NULL,
  `pid` varchar(36) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `nick` varchar(50) DEFAULT NULL,
  `avatar` varchar(150) DEFAULT NULL,
  `latitude` varchar(20) DEFAULT NULL,
  `longitude` varchar(20) DEFAULT NULL,
  `memo` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Calling` datetime DEFAULT NULL,
  `NoAnswer` datetime DEFAULT NULL,
  `Cancelled` datetime DEFAULT NULL,
  `Talking` datetime DEFAULT NULL,
  `call_length` int(11) DEFAULT NULL,
  `opid` int(2) DEFAULT NULL,
  `ext` varchar(4) DEFAULT NULL,
  `records` longtext,
  `endcode` int(1) DEFAULT NULL,
  `isUpdated` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`callid`),
  KEY `calling` (`Calling`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `resetpw_logs`
--

DROP TABLE IF EXISTS `resetpw_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `resetpw_logs` (
  `taskid` int(11) NOT NULL AUTO_INCREMENT,
  `opid` int(11) DEFAULT NULL,
  `reqTime` datetime DEFAULT NULL,
  `expTime` datetime DEFAULT NULL,
  `activedTime` datetime DEFAULT NULL,
  `taskno` varchar(64) NOT NULL,
  PRIMARY KEY (`taskid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `opid` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(50) DEFAULT NULL,
  `password` varchar(64) DEFAULT 'ce1075a75f242cc7441c69b630a42acdccad558219c495af04c66239239bce3c',
  `type` tinyint(1) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`opid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-26 17:01:59
