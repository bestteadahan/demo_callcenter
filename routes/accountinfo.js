/*
Endpoint: /accountinfo
Export modules:
    1. get()
        a. res.redirect('/login')
        b. res.render('accountinfo', { data: renderdata })

            Object : renderdata = {
                account: 使用者帳號
                type: 使用者role type
                isConnected: 使用者是否已跟voip連線
                ext: 紀錄該使用者已註冊之分機號
                info: 使用者帳號資料
            }

    2. post()
        a. 更新帳號資料
*/


const express = require('express')
const router = express.Router()
const aws = require('./controllers/AwsDB')
const debug = require('debug')('app:accountinfo')


router.get('/', (req, res) => {

    let sess,
        targetDB = req.app.locals.targetDB

    if (req.session.sess === undefined) {

        res.redirect('/login')
    } else {
        sess = JSON.parse(req.session.sess)

        if (sess.isVerified) {

            let renderdata = {
                navtitle: req.app.locals.navtitle,
                account: sess.account,
                type: sess.type,
                isConnected: sess.isConnected,
                ext: sess.ext,
                info: null
            }
            sess.page = 'accountinfo'
            req.session.sess = JSON.stringify(sess)

            aws.getConnection(targetDB, (err, conn) => {
                if (err) debug(err)

                conn.query(`SELECT account, type, firstname, lastname FROM users WHERE opid = ${sess.opid}`, (err, results) => {
                    if (err) debug(err)

                    renderdata.info = results[0]
                    res.render('accountinfo', { data: renderdata })

                }).on('end', () => {
                    conn.end()
                })
            })
        } else {
            res.redirect('/login')
        }
    }
})

router.post('/', (req, res) => {

    let sess = JSON.parse(req.session.sess),
        fname = req.body.fname,
        lname = req.body.lname,
        targetDB = req.app.locals.targetDB

    aws.getConnection(targetDB, (err, conn) => {
        if (err) debug(err)

        conn.query(`UPDATE users SET firstname = "${fname}", lastname = "${lname}" WHERE opid = ${sess.opid}`, (err, results) => {
            if (err) debug(err)
            debug(`Updated information, opid: ${sess.opid}`)

        }).on('end', () => {
            res.end()
        })
    })
})

module.exports = router