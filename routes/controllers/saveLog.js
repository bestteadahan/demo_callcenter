/*
Endpoint: /saveLog
Export modules:
    1. post() <= called by view/ajax
        a. 處理call log資料儲存
        b. res.json(Boolean)
*/

const express = require('express')
const router = express.Router()
const aws = require('./AwsDB')
const debug = require('debug')('app:saveLog')

router.post('/', (req, res) => {

    let sess

    if (req.session.sess === undefined) {
        res.json(false)
    } else {
        sess = JSON.parse(req.session.sess)
        debug(`opid: ${sess.opid} , ext: ${sess.ext}`)

        let targetDB = req.app.locals.targetDB,
            partialLog = req.body,
            fullLog

        // 將網頁回傳的 partialLog 與 API 取得的 historyLog 合併，並加入 ext, opid 資訊
        fullLog = Object.assign(partialLog, { ext: sess.ext, opid: sess.opid })

        aws.getConnection(targetDB, (err, conn) => {
            if (err) debug(err)

            // console.log(fullLog)
            Object.keys(fullLog).forEach(key=>{
                if (!fullLog[key]) {
                    fullLog[key] = null
                }
            })

            // 使用Replace，若資料已存在則update否則insert
            conn.query(`REPLACE INTO history_calls SET ?`, fullLog, (err, results) => {
                if (err) debug(err)
                res.json(true)
                debug(`Inserted/Updated call log, id: ${partialLog.callid}`)
            }).on('end', () => {
                conn.end()
            })
        })
    }
})

module.exports = router