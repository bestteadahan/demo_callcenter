/*
Export modules:
    1. parser(callHistoryLog)
        a. callHistoryLog: api取得的資料
        b. 回傳parse後的資料 (promise object)
*/


const debug = require('debug')('app:LogUpdater')


exports.parser = function (callHistoryLog) {
    return new Promise((resolve, reject) => {
        let historyLog

        try {
            historyLog = {
                Calling: null,
                NoAnswer: null,
                Talking: null,
                Cancelled: null
            }

            callHistoryLog.callLog.forEach(record => {
                let seperator = record.indexOf(':'),
                    key = record.slice(0, seperator),
                    val = record.slice(seperator + 1, record.length)
                historyLog[key] = val
            })

            resolve(historyLog)
        } catch (e) {
            reject(e)
        }
    })

}