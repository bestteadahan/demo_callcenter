/*
Export modules:
    1. getConnection(targetDB, callback)
        a. targetDB: 傳入連線目標DB名稱
        b. 回傳connection object
*/


const mysql = require('mysql');
const debug = require('debug')('app:aws')
const options = {
    user: 'root',
    password: 'demo1234',
    database: '',
    multipleStatements: true,
    timezone: 'utc'
}

exports.getConnection = function (targetDB, callback) {

    options.database = targetDB

    const conn = mysql.createConnection(options);

    // 處理connection error
    conn.connect(function (err) {
        if (err) {
            debug(`Connect to ${targetDB} fail!`);
            callback(err, null)
        }
        callback(null, conn)
    });
}