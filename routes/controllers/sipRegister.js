/*
Endpoint: /sipRegister
Export modules:
    1. get() <= called by view/ajax
        a. getExts: 取分機表 => res.json(extlist)
        b. getCid: 查詢cid => res.json(cid)
        c. getTicket: 取合法ticket => res.json(_ticket)

    2. post() <= called by view/ajax
        a. takeExt: 去db登記已使用分機
        b. untakeExt: 去db註銷現正使用分機
*/


const express = require('express')
const router = express.Router()
const aws = require('./AwsDB')
const debug = require('debug')('app:sipRegister')


router.get('/', function (req, res, next) {

    let uri = req.app.locals.voipServer,
        targetDB = req.app.locals.targetDB

    if (req.session.sess === undefined) {
        res.send(false)
    } else {
        let sess = JSON.parse(req.session.sess)

        if (sess.isVerified) {

            // debug('action: ' + req.query.action)
            switch (req.query.action) {

                // 確認使用者是否連線中
                case 'checkStatus':
                    res.json({ isConnected: sess.isConnected, ext: sess.ext, uri: uri })
                    break;

                // 取得可用分機
                case 'getExts':
                    let extlist
                    aws.getConnection(targetDB, (err, conn) => {
                        if (err) debug(err)
                        conn.query(`SELECT ext,cid FROM exts`, (err, results) => {
                            if (err) debug(err)
                            if (results.length != 0) {
                                extlist = results
                                res.json(extlist)
                                conn.end()
                            }
                        })
                    })
                    break;

                // 查詢該分機cid
                case 'getCid':
                    let cid,
                        ext = req.query.ext,
                        opid = sess.opid
                    // debug('from register: ', ext)

                    aws.getConnection(targetDB, (err, conn) => {

                        conn.query(`SELECT cid, available, opid FROM exts WHERE ext = '${ext}'`, (err, results) => {
                            if (err) debug(err)
                            cid = results[0].cid
                            res.json(cid)
                        }).on('end', () => {
                            conn.end()
                        })
                    })
                    break;

                // 向core server取ticket
                case 'getTicket':
                    let accesstoken = null,
                        _ticket = 'newTicket'

                    res.json(_ticket)
                    break;
            }
        }
    }
})


router.post('/', function (req, res, next) {

    let sess

    if (req.session.sess === undefined) {
        res.send(false)
    } else {

        sess = JSON.parse(req.session.sess)

        if (sess.isVerified) {

            let ext = req.body.ext,
                targetDB = req.app.locals.targetDB
            // debug('post ext: ' + ext)

            switch (req.body.action) {

                // 向db登錄該分機已有人使用
                case 'take':
                    aws.getConnection(targetDB, (err, conn) => {
                        if (err) debug(err)

                        conn.query(`UPDATE exts SET opid = null, available = 1 WHERE opid = '${sess.opid}' ; UPDATE exts SET opid = '${sess.opid}', available = 0 WHERE ext = '${ext}'`, (err, results) => {
                            sess.isConnected = true
                            sess.ext = ext
                            req.session.sess = JSON.stringify(sess)

                        }).on('end', () => {
                            res.send(true)
                            conn.end()
                        })
                    })
                    break;

                // 向db取消登錄該分機
                case 'untake':
                    aws.getConnection(targetDB, (err, conn) => {
                        if (err) debug(err)

                        conn.query(`UPDATE exts SET opid = null, available = 1 WHERE ext = '${ext}'`, (err, results) => {
                            sess.isConnected = false
                            sess.ext = ''
                            req.session.sess = JSON.stringify(sess)

                        }).on('end', () => {
                            res.send(true)
                            conn.end()
                        })
                    })
                    break;
            }
        }
    }
});

module.exports = router;