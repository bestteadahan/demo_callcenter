/*
Endpoint: /login
Export modules:
    1. get()
        a. 檢查/建立session
        b. res.redirect: /index
        c. res.render: login
        
    2. post() <= called by view/ajax
        a. 連線DB做帳密驗證
        b. res.json(Boolean)
*/

const express = require('express')
const router = express.Router()

const crypto = require('crypto')
const aws = require('./controllers/AwsDB')
const debug = require('debug')('app:login')



router.get('/', (req, res) => {

    let sess

    //檢查profile是否存在，如果不存在建立後產生登入頁面
    if (req.session.sess == undefined) {

        sess = {
            page: 'login',
            isVerified: false,
            opid: null,
            account: null,
            type: null,
            isConnected: false,
            ext: null
        }
        req.session.sess = JSON.stringify(sess)
        //產生登入頁面
        res.render('login')

    } else {

        // 若profile已存在
        sess = JSON.parse(req.session.sess)

        // 確認使用者是否完成登入驗證
        if (sess.isVerified) {
            //完成登入驗證的使用者先轉首頁
            res.redirect('/index')
        } else {
            //未完成登入驗證的使用者產生登入頁面
            res.render('login')
        }
    }
})

router.post('/', (req, res) => {

    let sess

    if (req.session.sess == undefined) {
        // session不存在回傳false
        res.json(false)

    } else {
        // session存在
        sess = JSON.parse(req.session.sess)

        let account = req.body.account,
            hashedpw = crypto.createHash('sha256').update(req.body.password).digest('hex'),  // 傳入之密碼做sha256加密
            targetDB = req.app.locals.targetDB,
            isSuccess

        // 帳密驗證
        aws.getConnection(targetDB, (err, conn) => {
            if (err) {
                debug(err)
                res.json(false)
            } else {

                // 查詢使用者帳號
                conn.query(`SELECT * FROM users WHERE account = '${account}' AND enabled = 1 LIMIT 1`, (err, results) => {
                    if (err) {
                        debug(err)
                        isSuccess = false
                    } else {

                        if (results.length === 1 && hashedpw === results[0].password) {
                            // 若該使用者存在且密碼正確
                            sess.isVerified = true
                            sess.account = account
                            sess.opid = results[0].opid
                            sess.type = results[0].type
                            sess.page = 'login'
                        } else {
                            // 驗證錯誤
                            sess.isVerified = false
                        }

                        isSuccess = sess.isVerified
                    }
                }).on('end', () => {
                    conn.end()
                    req.session.sess = JSON.stringify(sess)
                    res.json(isSuccess)
                })
            }
        })
    }
})

module.exports = router