/*
Endpoint: /forgetpw
Export modules:
    1. get()
        a. res.render: forgetpw
        
    2. post()
        a. 驗證帳號合法 => 於table: resetpw_logs新增一筆log
        
            Object: log = {
                taskno: hash當下時間，作為該筆log之id
                opid: 使用者id
                reqTime: 提出申請時間
                expTime: 密碼變更有效時間 (15分鐘)
            }

        b. 郵件自動發信，附上重設密碼link
*/


var express = require('express')
var router = express.Router()
const nodemailer = require('nodemailer');
const crypto = require('crypto')
const aws = require('./controllers/AwsDB')
const debug = require('debug')('app:forgetpw')


router.get('/', (req, res) => {
    res.render('forgetpw')
})

router.post('/', (req, res) => {

    // 新增變更密碼request log
    let targetDB = req.app.locals.targetDB,
        accountMail = req.body.mail.replace(/[\'\"]/g, ""),
        log = {
            taskno: crypto.createHash('sha256').update(Date.now().toString()).digest('hex'),
            opid: '',
            reqTime: { toSqlString: () => 'NOW()' },
            expTime: { toSqlString: () => 'DATE_ADD(NOW(), INTERVAL 15 MINUTE)' }
        }

    aws.getConnection(targetDB, (err, conn) => {
        if (err) debug(err)

        // 查詢該帳號是否存在
        conn.query(`SELECT opid FROM users WHERE account = '${accountMail}'`, (err, results) => {

            if (err) debug(err)

            if (results.length === 0) {

                // 帳號不存在
                debug('user doesn\'t exist.')
                conn.end()

            } else {

                // 帳號存在 => 取得opid
                log.opid = results[0].opid
                debug(`opid: ${log.opid} requested for reset pw, taskno: ${log.taskno}`)

                // 新增log
                conn.query(`INSERT INTO resetpw_logs SET ?`, log, (err, results) => {

                    if (err) debug(err)
                    debug(`Inserted ${results.affectedRows} reset pw task.`)

                }).on('end', () => {
                    conn.end()
                })
            }
        })
    })

    // 郵件自動發信，附上重設密碼link
    let host = req.body.host,
        targetside = `https://${host}/resetpw/${log.taskno}`,
        transporter = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
                user: 'dev@wattertek.com',
                pass: 'Wtk-1101'
            }
        }),
        // Set nodemailer options
        options = {
            from: 'dev@wattertek.com',  // 寄件者
            to: accountMail,  // 收件者
            // cc: 'account3@gmail.com',  // 副本
            // bcc: 'wattertech.inc@gmail.com;dagodalin@gmail.com',  // 密件副本
            subject: 'Reset your OFW dashboard\'s password',  // 主旨
            html: `
            <h2><span style="color: #0000ff;">RESET YOUR PASSWORD:</span></h2>
            <p>We've got your request, please click the link below:&nbsp;</p>
            <p><a href="${targetside}" target="_blank" rel="noopener">Reset my password</a></p>
            <p><span style="color: #ff0000;">This link will be expired in 15 min.</span></p>
            `
        };

    transporter.sendMail(options, function (err, info) {
        if (err) {
            debug(err);
        } else {
            debug('Sent reset mail OK');
        }
    });

    res.end()

})

module.exports = router