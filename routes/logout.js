/*
Endpoint: /logout
Export modules:
    1. get()
        a. 取消分機綁定
        b. 消滅session obj
        c. res.redirect: /login, /    
*/


const express = require('express')
const router = express.Router()
const aws = require('./controllers/AwsDB')
const debug = require('debug')('app:logout')


router.get('/', (req, res) => {
    if (req.session.sess != undefined) {
        let sess = JSON.parse(req.session.sess),
            ext = sess.ext,
            targetDB = req.app.locals.targetDB

        debug('user logout: ' + sess.ext)

        if (ext != '' && ext != null) {

            // 若該使用者目前有佔線，就修改DB狀態
            aws.getConnection(targetDB, (err, conn) => {

                if (err) debug(err)

                conn.query(`UPDATE exts SET available = 1, opid = null WHERE ext = ${ext}`, (err, results) => {
                    debug(`Ext no. ${ext} released`)
                })
            })
        }
        req.session.destroy((err) => {
            res.redirect('/login');
        })
    } else {
        res.redirect('/');
    }
})

module.exports = router