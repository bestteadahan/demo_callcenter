/*
Endpoint: /
Export modules:
    1. get()  
        a. 建立session
        
            Object: sess = {
                page: 來源頁
                isVerified: 該帳戶是否已認證
                opid: 使用者id
                account: 使用者帳戶
                type: 使用者role type
                isConnected: 使用者是否已跟voip連線
                ext: 紀錄該使用者已註冊之分機號
            }

        b. 根據user的role轉到相對應頁面
        c. res.redirect: /login, /hotline, /historycalls, /accountmanage

*/


const express = require('express');
const router = express.Router();
const debug = require('debug')('app:index')

router.get('/', (req, res) => {

    let sess;

    // 檢查profile是否存在，如果不存在建立後轉去做認證
    if (req.session.sess == undefined) {
        sess = {
            page: 'index',
            isVerified: false,
            opid: null,
            account: null,
            type: null,
            isConnected: false,
            ext: null
        }
        req.session.sess = JSON.stringify(sess)

        res.redirect('/login')

    } else {

        //從session取得profile，並檢查是否通過認證，如果不是轉去做認證
        sess = JSON.parse(req.session.sess)

        if (sess.isVerified == true) {

            sess.page = 'index';
            req.session.sess = JSON.stringify(sess);

            switch (sess.type) {
                case 3:
                    res.redirect('/hotline') //Operator
                    break;
                case 2:
                    res.redirect('/historycalls') //Manager
                    break;
                case 1:
                    res.redirect('/accountmanage') //Admin
                    break;
                default:
                    res.redirect('/login')
            }

        } else {
            res.redirect('/login')
        }

    }

})

module.exports = router