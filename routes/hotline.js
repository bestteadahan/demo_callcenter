/*
Endpoint: /hotline
Export modules:
    1. get()
        a. 檢查session
        b. res.redirect: /login
        c. res.render('hotline', { data: renderdata })

            Object: renderdata = {
                account: 使用者帳號
                type: 使用者role
                isConnected: 使用者是否已跟voip連線
                ext: 紀錄該使用者已註冊之分機號
            }

    2. post() <= called by view/ajax
        a. res.redirect: 'login'
        b. res.json(Object)
*/


const express = require('express')
const router = express.Router()
const debug = require('debug')('app:hotline')



router.get('/', (req, res) => {

    let sess, renderdata

    if (req.session.sess === undefined) {

        res.redirect('/login')

    } else {

        sess = JSON.parse(req.session.sess)

        if (sess.isVerified) {

            renderdata = {
                navtitle: req.app.locals.navtitle,
                account: sess.account,
                type: sess.type,
                isConnected: sess.isConnected,
                ext: sess.ext
            }
            sess.page = 'hotline'
            req.session.sess = JSON.stringify(sess)
            res.render('hotline', { data: renderdata })
        } else {
            res.redirect('login')
        }
    }
})

router.post('/', (req, res) => {

    if (req.session.sess === undefined) {
        res.redirect('/login')
    } else {
        sess = JSON.parse(req.session.sess)
        // callid = req.body.callid

        let demo = [
            callerInfo1 = {
                id: 1,
                name: 'Jack Miller',
                nick: 'Bartender',
                avatar: 'demo_caller1.jpg'
            },
            callerInfo2 = {
                id: 2,
                name: 'Emma Watson',
                nick: 'Hermione',
                avatar: 'demo_caller2.jpg'
            }
        ]

        res.json(demo[parseInt(Math.round(Math.random()))])
    }
})

module.exports = router