/*
Endpoint: /accountmanage
Export modules:
    1. get()
        a. 查詢並回傳所有user (除了admin)
        b. res.redirect('/login')
        c. res.render('accountmanage', { data: renderdata })

            Object: renderdata = {
                account: 使用者帳號
                type: 使用者role type
                isConnected: 使用者是否已跟voip連線
                ext: 紀錄該使用者已註冊之分機號
                users: 使用者清單
            }

    2. post() <= called by view/ajax
        a. changeStatus: 修改帳號資料
        b. createAccount: 建立新帳號
        c. deleteAccount：刪除帳號
*/


const express = require('express')
const router = express.Router()
const aws = require('./controllers/AwsDB')
const debug = require('debug')('app:accountmanage')



router.get('/', (req, res) => {

    let sess,
        targetDB = req.app.locals.targetDB

    if (req.session.sess === undefined) {
        res.redirect('/login')
    } else {
        sess = JSON.parse(req.session.sess)
        sess.page = 'accountmanage'
        req.session.sess = JSON.stringify(sess)

        if (sess.isVerified) {

            let renderdata = {
                navtitle: req.app.locals.navtitle,
                account: sess.account,
                type: sess.type,
                users: [],
                isConnected: sess.isConnected,
                ext: sess.ext
            }

            aws.getConnection(targetDB, (err, conn) => {
                if (err) debug(err)

                conn.query(`SELECT opid, account, type, lastname, firstname, enabled FROM users WHERE type != 1`, (err, results) => {
                    renderdata.users = JSON.stringify(results)

                }).on('end', () => {
                    res.render('accountmanage', { data: renderdata })
                    conn.end()
                })
            })

        } else {
            res.redirect('/login')
        }
    }
})

router.post('/', (req, res) => {
    let opid, enabled, role, newUser,
        targetDB = req.app.locals.targetDB
    const types = {
        'Operator': 3,
        'Manager': 2,
        'Admin': 1
    }
    switch (req.body.action) {
        case 'changeStatus':
            opid = req.body.opid
            enabled = req.body.enabled
            type = req.body.type
            // debug('from server: ',opid,enabled,type)

            aws.getConnection(targetDB, (err, conn) => {
                if (err) debug(err)

                conn.query(`UPDATE users SET enabled = ${enabled}, type = ${type} WHERE opid = ${opid}`, (err, results) => {
                    if (err) debug(err)
                    debug(`Updated ${results.affectedRows} user, id: ${opid}`)
                }).on('end', () => {
                    res.end()
                })
            })
            break;
        case 'createAccount':
            role = req.body.role
            newUser = {
                account: req.body.account,
                firstname: req.body.fname,
                lastname: req.body.lname,
                type: types[role]
            }
            aws.getConnection(targetDB, (err, conn) => {
                if (err) debug(err)

                conn.query(`INSERT INTO users SET ?`, newUser, (err, results) => {
                    if (err) debug(err)
                    debug(`Inserted ${results.affectedRows} user`)
                }).on('end', () => {
                    res.end()
                })
            })
            break;
        case 'deleteAccount':
            opid = req.body.opid
            aws.getConnection(targetDB, (err, conn) => {
                if (err) debug(err)

                conn.query(`DELETE FROM users WHERE opid = ${opid}`, (err, results) => {
                    if (err) debug(err)
                    debug(`Deleted ${results.affectedRows} user, id: ${opid}`)
                }).on('end', () => {
                    res.end()
                })
            })
            break;
    }
})

module.exports = router