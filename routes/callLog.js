/*
Endpoint: /callLog
Export modules:
    1. get()
        a. res.redirect('/login')
        b. 從DB取回該筆log資訊
        C. res.render('callLog', { data: renderdata })

            Object: renderdata = {
                account: 使用者帳號
                type: 使用者role type
                isConnected: 使用者是否已跟voip連線
                ext: 紀錄該使用者已註冊之分機號
                log: 該筆log詳細資訊
            }
        
    2. post() <= called by view/ajax
        a. 更新DB
        b. res.json(Boolean)
*/


const express = require('express')
const router = express.Router()
const aws = require('./controllers/AwsDB')
const loop = require('./controllers/loop')
const updater = require('./controllers/LogUpdater')
const debug = require('debug')('app:callLog')



router.get('/', (req, res) => {

    let sess, data

    if (req.session.sess === undefined) {
        res.redirect('/login')
    } else {
        sess = JSON.parse(req.session.sess)

        if (sess.isVerified) {

            // 紀錄session status
            sess.page = 'callLog'
            req.session.sess = JSON.stringify(sess)

            let targetDB = req.app.locals.targetDB,
                callid = req.query.callid,
                renderdata = {
                    navtitle: req.app.locals.navtitle,
                    account: sess.account,
                    type: sess.type,
                    isConnected: sess.isConnected,
                    ext: sess.ext,
                    log: null
                }

            aws.getConnection(targetDB, (err, conn) => {
                if (err) debug(err)

                conn.query(`SELECT * FROM history_calls AS h LEFT JOIN users AS u ON h.opid = u.opid WHERE h.callid = '${callid}'`, (err, results) => {
                    if (err) debug(err)

                    renderdata.log = results[0]
                    res.render('callLog', { data: renderdata })
                }).on('end', () => {
                    conn.end()
                })
            })
        } else {
            res.redirect('/login')
        }
    }
})

router.post('/', (req, res) => {


    // 進行更新
    if (req.session.sess === undefined) {
        res.json(false)
    } else {

        let accesstoken,
            typecode = req.app.locals.coreserver,
            appid = req.app.locals.appid,
            appkey = req.app.locals.appkey,
            agid = req.app.locals.agid,
            scope = req.app.locals.scope,
            targetDB = req.app.locals.targetDB,
            callid = req.body.callid,
            logdata

        // 取access token
        loop.getAppAccessToken(typecode, appid, appkey, agid, scope, (err, response, body) => {
            if (err) debug(err)
            accesstoken = JSON.parse(response).access_token

            // 取callHistoryLog from core server API
            loop.getCallHistoryLog(typecode, accesstoken, callid, (err, callHistoryLog) => {
                if (err) debug(err)

                if (callHistoryLog != null) {


                    // 將取得資料做格式轉換
                    updater.parser(callHistoryLog)
                        .then(logdata => {

                            // debug('after parse: ', logdata)
                            logdata.isUpdated = 1

                            aws.getConnection(targetDB, (err, conn) => {
                                if (err) debug(err)

                                // 更新資料庫資料，標記為"已更新"
                                conn.query(`UPDATE history_calls SET ? WHERE callid = '${callid}'`, logdata, (err, results) => {
                                    if (err) debug(err)
                                    debug(`Updated ${results.affectedRows} call log`)
                                    res.json(true)
                                }).on('end', () => {
                                    conn.end()
                                })
                            })
                        })
                        .catch(err => {
                            debug(err)
                            res.json(false)
                        })

                } else {
                    debug('(callLog.js) Update fail')
                    res.json(false)
                }
            })
        })
    }
})

module.exports = router