/*
Endpoint: /resetpw
Export modules:
    1. get()
        a. res.render('resetpw', { isValid: Boolean })
        b. res.redirect: /login

    2. post() <= called by view/ajax
        a. table: users 更新密碼 => table: resetpw_logs 更新變更密碼時間
        b. res.json(Boolean)
*/


const express = require('express')
const router = express.Router()
const crypto = require('crypto')
const aws = require('./controllers/AwsDB')
const debug = require('debug')('app:resetpw')



router.get('/:taskno', (req, res) => {

    let taskno = req.params.taskno,  // 取得query string: taskno
        targetDB = req.app.locals.targetDB

    // 如果taskno字串長度有64再去DB查詢，否則直接跳轉login
    if (taskno.length === 64) {

        aws.getConnection(targetDB, (err, conn) => {

            if (err) debug(err)

            // log查詢條件：taskno正確、尚未修改過、變更期限尚未過期
            conn.query(`SELECT * FROM resetpw_logs WHERE taskno = '${taskno}' && activedTime IS NULL && NOW() between reqTime and expTime`, (err, results) => {

                if (err) debug(err)

                if (results.length != 0) {
                    res.render('resetpw', { isValid: true })
                } else {
                    res.render('resetpw', { isValid: false })
                }

            }).on('end', () => {
                conn.end()
            })
        })

    } else {
        res.redirect('/login')
    }
})

router.post('/', (req, res) => {

    let newpw = crypto.createHash('sha256').update(req.body.newpw).digest('hex'),  // hash pw
        taskno = req.body.taskno,
        isSuccess,
        targetDB = req.app.locals.targetDB

    aws.getConnection(targetDB, (err, conn) => {
        if (err) {
            debug(err)
            res.json(false)
        } else {

            // Update password in db
            conn.query(`UPDATE users SET password = '${newpw}' WHERE opid = (SELECT opid FROM resetpw_logs WHERE taskno = '${taskno}')`, (err, results) => {
                if (err) {
                    debug(err)
                    res.json(false)
                } else {
                    
                    debug(`Update pw success, taskno: ${taskno}`)

                    //Save reset log
                    conn.query(`UPDATE resetpw_logs SET activedTime = NOW() WHERE taskno = '${taskno}'`, (err, results) => {
                        if (err) {
                            debug(err)
                            isSuccess = false
                        } else {
                            debug(`Saved log`)
                            isSuccess = true
                        }
                    }).on('end', () => {
                        conn.end()
                        debug('results: ' + isSuccess)
                        res.json(isSuccess)
                    })
                }
            })
        }
    })
})

module.exports = router