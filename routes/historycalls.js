/*
Endpoint: /historycalls
Export modules:
    1. get()
        a. res.redirect('/login')
        b. res.render('historycalls', { data: renderdata })

            Object: renderdata = {
                account: 使用者帳號
                type: 使用者role type
                isConnected: 使用者是否已跟voip連線
                ext: 紀錄該使用者已註冊之分機號
                totalCounts: 全部資料筆數
                countsPerPage: 每頁顯示的資料筆數
                oplist: 所有operator + manager列表 (first name)
            }

    2. post()
        a. Refresh新的來電log總數 => view pagination頁數顯示
        b. 取得某一頁的log list
        c. res.json(Object)
*/


const express = require('express')
const router = express.Router()
const loop = require('./controllers/loop')
const aws = require('./controllers/AwsDB')
const debug = require('debug')('app:historycalls')



router.get('/', (req, res) => {

    let sess

    if (req.session.sess === undefined) {
        res.redirect('/login')
    } else {

        sess = JSON.parse(req.session.sess)

        if (sess.isVerified) {

            // 紀錄session status
            sess.page = 'historycalls'
            req.session.sess = JSON.stringify(sess)

            let targetDB = req.app.locals.targetDB,
                query,
                renderdata = {
                    navtitle: req.app.locals.navtitle,
                    account: sess.account,
                    type: sess.type,
                    isConnected: sess.isConnected,
                    ext: sess.ext,
                    totalCounts: null,
                    countsPerPage: 8,
                    oplist: []
                }

            // 查詢歷史來電log數
            aws.getConnection(targetDB, (err, conn) => {

                if (err) debug(err)

                if (renderdata.type == 3) {

                    // 使用者為Operator => 只查詢該op之歷史log數
                    query = `SELECT COUNT(callid) AS totalCounts FROM history_calls WHERE opid = ${sess.opid}`
                    conn.query(query, (err, results) => {

                        if (err) debug(err)
                        renderdata.totalCounts = results[0].totalCounts
                        debug('Number of total logs: ', renderdata.totalCounts)

                    }).on('end', () => {
                        conn.end()
                        res.render('historycalls', { data: renderdata })
                    })

                } else {

                    // 使用者為Manager => 查詢所有log總數、所有op & manager名單
                    query = `SELECT COUNT(callid) AS totalCounts FROM history_calls ; SELECT firstname FROM users WHERE opid IN (SELECT distinct(opid) FROM history_calls)`
                    conn.query(query, (err, results) => {

                        if (err) debug(err)
                        renderdata.totalCounts = results[0][0].totalCounts
                        renderdata.oplist = results[1].map(ele => ele.firstname)
                        debug('Get total number of logs: ', renderdata.totalCounts)
                        debug('Get op list.')

                    }).on('end', () => {
                        conn.end()
                        res.render('historycalls', { data: renderdata })
                    })

                }

            })

        } else {
            res.redirect('/login')
        }
    }
})


router.post('/', (req, res) => {
    let sess

    if (req.session.sess === undefined) {
        res.redirect('login')
    } else {

        sess = JSON.parse(req.session.sess)

        if (sess.isVerified) {

            let targetDB = req.app.locals.targetDB,
                action = req.body.action,
                opfirstname = req.body.opfirstname,  // 目前篩選的operator名
                countsPerPage = 8,  // 每頁顯示資料筆數
                skip = (req.body.currentPage - 1) * countsPerPage,  // 查詢跳過筆數
                whereClause,
                query

            if (action == 'getNewTotalCounts') {

                // Refresh新的來電log總數
                aws.getConnection(targetDB, (err, conn) => {
                    if (err) debug(err)

                    // 建立SQL where子句
                    if (opfirstname == 'all') {
                        whereClause = ""
                    } else {
                        whereClause = `WHERE opid IN (SELECT opid FROM users WHERE firstname = '${opfirstname}')`
                    }

                    query = `SELECT COUNT(callid) AS totalCounts FROM history_calls ${whereClause}`

                    conn.query(query, (err, results) => {
                        if (err) debug(err)
                        debug('Get total number of logs: ', results[0].totalCounts)
                        res.json(results[0].totalCounts)
                    }).on('end', () => {
                        conn.end()
                    })
                })

            } else {

                // 取得某一頁的log list
                aws.getConnection(targetDB, (err, conn) => {
                    if (err) debug(err)

                    if (sess.type == 3) {

                        // 若使用者為operator => 只查詢該opid資料
                        whereClause = `WHERE h.opid = ${sess.opid}`

                    } else {

                        // 若使用者為manager or admin => 查詢使用者篩選的op資料
                        if (opfirstname == 'all') {
                            whereClause = ""
                        } else {
                            whereClause = `WHERE u.firstname = "${opfirstname}"`
                        }
                    }

                    query = `SELECT * FROM history_calls AS h LEFT JOIN users AS u ON h.opid = u.opid ${whereClause} ORDER BY h.created DESC , h.callin DESC LIMIT ${skip} , ${countsPerPage}`

                    conn.query(query, (err, results) => {
                        if (err) debug(err)
                        res.json(results)
                    }).on('end', () => {
                        conn.end()
                    })
                })
            }
        }
    }
})
module.exports = router