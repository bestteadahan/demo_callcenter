$(document).ready(function(){
    
    // 提示alert
    function popupNotice(title, msg) {
        $("#notice").addClass("show")
        $(".alert-heading").text(title)
        $("#notice p").text(msg)
        setTimeout(function () {
            $("#notice").removeClass("show")
        }, 4000)
    }

    // 申請變更密碼
    $("#apply").click(function () {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/forgetpw',
            data: {
                mail: $("#account").val(),
                host: location.host
            }
        }).done(
            popupNotice("Apply Success", "Please check your mailbox")
        )
    })

    // 儲存變更個人資料
    $("#save").click(function () {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/accountinfo',
            data: {
                account: $("#account").val(),
                fname: $("#fname").val(),
                lname: $("#lname").val()
            }
        }).done(
            popupNotice("Save Success", "User information has been updated")
        )
    })
})