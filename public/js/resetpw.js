$(document).ready(function () {
    // 檢查兩次密碼是否輸入一致
    function checkpw() {
        if ($("#newpw").val() !== $("#pwc").val()) {
            $("#pwc").addClass("is-invalid")
            $("#newpw").addClass("is-invalid")
            $("#reset").prop("disabled", true)
            return false
        } else {
            $("#pwc").removeClass("is-invalid").addClass("is-valid")
            $("#newpw").removeClass("is-invalid").addClass("is-valid")
            $("#reset").prop("disabled", false)
            return true
        }
    }

    $("#pwc").keyup(checkpw)
    $("#newpw").keyup(checkpw)

    // 按下重設密碼後post ajax
    $("#reset").click(function () {
        if (checkpw()) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/resetpw',
                data: {
                    taskno: location.pathname.split('/')[2],
                    newpw: $("#newpw").val()
                }
            }).done(function (isSuccess) {
                if (isSuccess) {
                    $("#notice").modal({ show: true })
                } else {
                    $("#error .modal-body").text("Something wrong, please try again later")
                    $("#error").modal({ show: true })
                }
            })
        }
    })
})