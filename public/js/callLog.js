$(document).ready(function () {

    // 變更日期時間顯示格式
    function changeDT(dt) {
        if (dt) {
            let new_dt = new Date(dt)
            return moment.utc(new_dt).format("YYYY-M-D H:mm:ss")
        }
    }

    // 顯示datetime
    $('#callin').text(changeDT(callin))
    $('#answer').text(changeDT(answer))
    $('#reject').text(changeDT(reject))
    $('#hangup').text(changeDT(hangup))

    // 將符號轉html (ex: '&lt;' 轉 '<' )，再取成文字內容
    if (memo != "") {
        var myt = $.parseHTML(memo)[0].textContent
        $('#memo').append($(myt))
    }

    // 繪製地圖
    if (lat == "" || lng == "") {
        $('#map').append(`<h5 class="m-auto align-self-center">Caller doesn't provide location info</h5>`)
    } else {
        $('#map').createMap({ location: { lat: parseFloat(lat), lng: parseFloat(lng) } })
    }

    // 來電狀態
    switch (endcode) {
        case (1):
            $('#endcode').text('Missed. Timeout/End by caller')
            break;
        case (3):
            $('#endcode').text('Missed. Rejected by op')
            break;
        case (4):
            $('#endcode').text('Catched. End by caller')
            break;
        case (5):
            $('#endcode').text('Catched. End by op')
            break;
        default:
            $('#endcode').text('Others')
            break;
    }
})