$(document).ready(function () {
  // 左側nav收折
  $('#sidenavToggler').click(function () {
    $('body').toggleClass('sidenav-toggled')
    $('.mybrand').toggleClass("invisible")
    $('.user-panel').toggleClass("invisible")
    $('.navbar-sidenav .nav-link-collapse').toggleClass('collapsed');
    $('.navbar-sidenav .sidenav-second-level').toggleClass('show');
  })
})