(function ($) {
    "use strict"
    $.widget("loop.emcall", {
        // 設定參數
        options: {
            state: null,  // 狀態
            ext: null,  // 分機碼
            cid: null,  // 用戶端識別碼 (存於DB: exts table內)
            opstatus: null,  // op在線狀態(navbar.pug)
            password: null,  // Equal to Ticket
            audioRing: null,  // 播放響鈴用容器
            connect: null,  // 連線按鈕
            answer: null,  // 回應按鈕
            authser: '/sipRegister',  // 後台取ticket路徑
            reject: null,  // 斷線按鈕
            hangup: null,  // 掛斷按鈕
            call: null,  // 外撥按鈕
            save: null,  // 儲存按鈕
            simulate: null,
            dialog: null,  // 進線通知
            error: null,  // 錯誤通知popup
            errormsg: null,  // 錯誤通知訊息
            register_expires: 600,  // 登錄過期時間
            ua: null,  // JsSIP UA物件
            callerInfo: null,  // api 查詢之caller資料
            call_log: {
                callid: null,
                memo: null,  //memo editor內容
                endcode: null,  // 結束通話原因代碼
                callin: null,
                answer: null,
                reject: null,
                hangup: null,
            },
            memoEditor: null,
            timer: null,
            notification: null  // chrome popup通知
        },
        // 建立VOIP環境
        _create: function () {
            this.options.audioRing = $('audio.loop-voip-ringplayer')[0]
            this.options.connect = $('button.loop-voip-connect')
            this.options.answer = $('button.loop-voip-answer')
            this.options.reject = $('button.loop-voip-reject')
            this.options.hangup = $("#hangup")
            this.options.save = $('#save')
            this.options.simulate = $("#simulate")
            this.options.call = $('button.loop-voip-call')
            this.options.dialog = $('#call')
            this.options.error = $('#error')
            this.options.errormsg = $('#errormsg')
            this.options.opstatus = $("#opstatus")
            this.options.callerInfo = {
                name: $('#name'),
                nick: $('#nick'),
                avatar: $('#avatar')[0],
                map: $('#map'),
                callerstatus: $('#callerstatus'),
                callindt: $('#callin'),
                lastcall: $('#lastcall')
            }

            if ($("#editor").length != 0) {
                this.options.memoEditor = CKEDITOR.replace('editor', {
                    language: 'en',
                    // height: 206,
                    resize_enabled: false,
                    removeButtons: 'Subscript,Superscript,Anchor,About'
                })
            }

            this._setRing()
            this._setConnectBtn()
            this._setAnswerBtn()
            this._setRejectBtn()
            this._setHangupBtn()
            this._setSaveBtn()
            this._setSimulateBtn()

            // 刷新分機清單
            this.checkExt()

            // 若初始化時有綁分機，則自動連線
            if (this.options.ext != null) {
                this._prepareToConnect()
            }

            //每一小時更新Ticket一次
            let g = this
            setInterval(function () {
                g._getTicket()
                    // 取得ticket成功
                    .then((ticket) => {
                        // 將ticket紀錄於options內
                        g.options.password = ticket
                    })
                    // 取得ticket失敗
                    .catch(() => {
                        g.options._showErrorMsg('After 1 hr, renew ticket from core server error.')
                    })
            }, 3600000)
        },
        // 去後台取ticket
        _getTicket: function () {
            return new Promise((resolve, reject) => {
                let g = this
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: g.options.authser,
                    data: { action: 'getTicket' }
                }).done(function (ticket) {
                    if (ticket) {
                        resolve(ticket)
                    } else {
                        reject()
                    }
                })
            })
        },
        // 取得分機的cid
        _getCid: function (ext) {
            return new Promise((resolve, reject) => {
                let g = this
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: g.options.authser,
                    data: {
                        action: 'getCid',
                        ext: ext
                    }
                }).done(function (cid) {
                    if (cid) {
                        // 將分機紀錄在options內
                        g.options.ext = ext
                        resolve(cid)
                    } else {
                        reject()
                    }
                })
            })
        },
        // 設定connect按鈕事件
        _setConnectBtn: function () {
            let g = this
            if (this.options.connect) {
                this.options.connect.click(function () {
                    // 確認是否有user agent (ua) object
                    if (!g.options.ua) {
                        g.options.ext = $("#exts :selected").val()
                        g._prepareToConnect()
                    } else {
                        // 有user agent、取消註冊、清空分機
                        g._disconnect()
                    }
                })
            } else {
                console.log('No connect button !')
            }
        },
        // 連線前置工作；取cid, ticket並建立連線
        _prepareToConnect: function () {
            let g = this
            g._getCid(g.options.ext)
                // 取得cid成功
                .then((cid) => {
                    // 將cid紀錄於options內
                    g.options.cid = cid
                    console.log('Get cid: ' + cid)
                    // 取ticket
                    g._getTicket()
                        // 取得ticket成功
                        .then((ticket) => {
                            // 將ticket紀錄於options內
                            g.options.password = ticket
                            g._connect()
                        })
                        // 取得ticket失敗
                        .catch(() => {
                            g._showErrorMsg('Cannot get ticket from core server.')
                        })
                })
                // 取得cid失敗
                .catch(() => {
                    g._showErrorMsg('This ext. has been used, plase choose another.')
                    g.checkExt()
                })
        },
        // 設定answer按鈕事件
        _setAnswerBtn: function () {
            let g = this
            if (g.options.answer) {
                g.options.answer.click(function () {
                    g._answer()
                    g.options.notification.close()
                })
            } else {
                console.log('No answer button !')
            }
        },
        // 設定Reject按鈕事件
        _setRejectBtn: function () {
            let g = this
            if (g.options.reject) {
                g.options.reject.click(function (e) {
                    g._reject(e)
                    g.options.hangup.hide()
                    g.options.notification.close()
                    g.options.state = 'failed'
                    g._audioStop('ringing')
                    g.options.dialog.modal('hide')
                    g._removeCallerInfo()
                })
            } else {
                console.log('No reject button !')
            }
        },
        // 設定Hangup按鈕事件
        _setHangupBtn: function () {
            let g = this
            if (g.options.hangup) {
                g.options.hangup.click(function (e) {
                    console.dir(e)
                    g._reject(e)
                    g.options.hangup.hide()
                    // 結束通話後儲存本次來電，並且enabled存檔按鈕
                    g._saveLog()
                    g.options.save.removeClass('btn-secondary').addClass('btn-primary').prop('disabled', false)
                })
            } else {
                console.log('No reject button !')
            }
        },
        // 設定save按鈕事件
        _setSaveBtn: function () {
            let g = this
            if (g.options.save) {
                g.options.save.click(function () {
                    g._removeCallerInfo()
                })
            } else {
                console.log('No save button !')
            }
        },
        // 設定鈴響
        _setRing: function () {
            let g = this
            $.ajax({
                type: 'GET',
                dataType: '',
                url: "js/sounds.json",
                success: function (data) {
                    $.each(data, function (key, val) {
                        if (key === 'ringing') {
                            if (g.options.audioRing != undefined) {
                                g.options.audioRing.src = val
                            } else {
                                console.log('No ring player, audio object undefined !')
                            }
                        }
                    })
                }
            })
        },
        // 設定connect作業
        _connect: function () {
            let g = this
            g.options.state = 'connecting'
            g.options.ua = true
            g._takeExt()
            g.options.state = 'registered'
        },
        _setSimulateBtn: function () {
            let g = this
            if (g.options.simulate) {
                g.options.simulate.click(function () {
                    //取得新值
                    g._audioPlay('ringing')
                    g.options.dialog.modal('show')
                    // 清空目前畫面
                    g._removeCallerInfo()
                    // 來電推播通知
                    g._notify()
                    // 帶入目前caller資訊
                    g._renderCallerInfo()
                })
            }
        },
        // 取消與voip之連線
        _disconnect: function () {
            let g = this
            g._untakeExt()
            if (g.options.ua) {
                g.options.ua = null
            }
        },
        // 設定Answer作業
        _answer: function () {
            let g = this
            g.options.call_log.answer = moment().format("YYYY-M-D H:mm:ss")
            g.options.state = 'confirmed'
            // 顯示掛斷按鈕及計時器
            g.options.hangup.show()
            g._timer()
            this._audioStop('ringing')
        },
        // 設定Reject作業
        _reject: function (e) {
            let g = this
            if (e.currentTarget.id == "hangup") {
                g.options.call_log.endcode = 5
                g.options.call_log.hangup = moment().format("YYYY-M-D H:mm:ss")
            } else {
                g.options.call_log.endcode = 3
                g.options.call_log.reject = moment().format("YYYY-M-D H:mm:ss")
            }
            this._audioStop('ringing')
        },
        // 播放聲音
        _audioPlay: function (type) {
            if (type === 'ringing') {
                this.options.audioRing.loop = true
                this.options.audioRing.play()
            }
        },
        // 停播聲音
        _audioStop: function (type) {
            if (type === 'ringing') {
                this.options.audioRing.pause()
            }
        },
        // 去db登記已使用分機
        _takeExt: function () {
            let g = this
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: g.options.authser,
                data: {
                    action: 'take',
                    ext: g.options.ext
                }
            }).done(function (isSuccess) {
                if (isSuccess) {
                    console.log('Take ext: ' + g.options.ext)
                    g.checkExt()

                    if (g.options.connect.hasClass('btn-primary')) {
                        g.options.connect.toggleClass("btn-primary btn-danger")
                    }
                    g.options.connect.text('Disconnect')
                    g.options.opstatus.text(`Online Ext: ${g.options.ext}`).toggleClass("badge-secondary badge-primary")
                }
            })
        },
        // 去db註銷現正使用分機
        _untakeExt: function () {
            let g = this
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: g.options.authser,
                data: {
                    action: 'untake',
                    ext: g.options.ext
                }
            }).done(function (isSuccess) {
                if (isSuccess) {
                    console.log('Untake ext: ' + g.options.ext)
                    g.options.ext = null
                    g.options.cid = null
                    g.checkExt()
                    if (g.options.connect.hasClass('btn-danger')) {
                        g.options.connect.toggleClass("btn-danger btn-primary")
                    }
                    g.options.connect.text('Connect')
                    g.options.opstatus.text('Offline').toggleClass("badge-primary badge-secondary")
                }
            })
        },
        // 推播通知
        _notify: function () {
            let g = this
            // 推播通知設定
            var notifyConfig = {
                body: 'OFW calling now!', // 設定內容
                icon: '/img/phone.png', // 設定 icon
                tag: 'newCall-in'
            }
            // 建立推播通知物件
            g.options.notification = new Notification('OFW EMER Notification', notifyConfig)
            g.options.notification.onclick = function (e) { // 綁定點擊事件
                e.preventDefault() // prevent the browser from focusing the Notification's tab
                window.focus()
                g.options.notification.close()
            }
            window.onfocus = function () {
                g.options.notification.close()
            }
            navigator.mediaDevices.getUserMedia({ video: false, audio: true })
        },
        // 接聽後hangup計時
        _timer: function () {
            let g = this
            // 先清除舊的timer
            if (g.options.timer != undefined) {
                clearInterval(g.options.timer)
            }
            let total = 0
            let minutesLabel = $("#minutes")
            let secondsLabel = $("#seconds")
            // 歸零
            minutesLabel.text('00')
            secondsLabel.text('00')
            // 設定每秒更新
            g.options.timer = setInterval(setTime, 1000)
            function setTime() {
                total++
                minutesLabel.text(pad(parseInt(total / 60)))
                secondsLabel.text(pad(total % 60))
            }
            // 兩位數補0
            function pad(val) {
                let valString = val + ""
                if (valString.length < 2) {
                    return "0" + valString
                } else {
                    return valString
                }
            }

        },
        // 更新/顯示caller資訊
        _renderCallerInfo: function () {
            let g = this
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/hotline'
            }).done(function (callerInfo) {
                if (callerInfo != null) {

                    g.options.call_log.callid = g._uuid()
                    g.options.call_log.callin = moment().format("YYYY-M-D H:mm:ss")

                    // 頁面Status區塊內資訊
                    g.options.callerInfo.callindt.prop('placeholder', moment().format("YYYY-M-D H:mm:ss"))
                    g.options.callerInfo.callerstatus.removeClass('badge-secondary').addClass('badge-primary').text('Processing')

                    // Caller姓名資訊
                    g.options.callerInfo.name.text(`${callerInfo.name}`)
                    g.options.callerInfo.nick.text(callerInfo.nick)

                    // Caller頭像
                    if (callerInfo.avatar != null) {
                        g.options.callerInfo.avatar.src = `/img/${callerInfo.avatar}`
                    }

                    let lat, lng

                    // 繪製地圖
                    navigator.geolocation.getCurrentPosition(position => {
                        lat = position.coords.latitude
                        lng = position.coords.longitude
                        g.options.callerInfo.map.createMap("setMap", { lat: lat, lng: lng })

                        // 將取得之callerinfo資料，合併暫存至options.call_log
                        Object.assign(g.options.call_log, {
                            pid: callerInfo.id,
                            name: callerInfo.name,
                            nick: callerInfo.nick,
                            avatar: callerInfo.avatar,
                            latitude: lat,
                            longitude: lng
                        })
                    })

                    console.log('_renderCallerInfo (New log): ', g.options.call_log)
                } else {
                    console.log('Session error! Please re-login')
                }
            })
        },
        // 清空/移除頁面caller資訊
        _removeCallerInfo: function () {
            let g = this

            console.log('Start to erase current log')

            // 清空前將所有資料儲存
            g._saveLog()

            // 隱藏掛斷按鍵
            g.options.hangup.hide()

            g.options.callerInfo.callindt.prop('placeholder', '')
            g.options.callerInfo.callerstatus.removeClass('badge-primary').addClass('badge-secondary').text('No call')
            g.options.callerInfo.name.text('')
            g.options.callerInfo.nick.text('')
            g.options.callerInfo.avatar.src = '/img/caller.png'
            g.options.callerInfo.map.createMap("hide")
            g.options.memoEditor.setData('')

            g.options.save.removeClass('btn-primary').addClass('btn-secondary').prop('disabled', true)
            g.options.call_log.callid = null
        },
        // 更新分機清單
        checkExt: function () {
            let g = this

            if (g.options.ext != null) {
                $("#loop-call .card-body").hide()
            } else {
                $("#loop-call .card-body").show()
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: g.options.authser,
                    data: { action: 'getExts' }
                }).done(function (exts) {
                    $("#exts option").remove()
                    if (exts.length != 0) {
                        exts.forEach(function (ext) {
                            $("#exts").append($("<option>", {
                                value: ext.ext,
                                text: ext.ext
                            }))
                        })
                        $("#exts").prop("disabled", false)
                        $("#connect").prop("disabled", false)
                        $("#extmsg").text("Choose ext.")
                    } else {
                        $("#exts").prop("disabled", true)
                        $("#connect").toggleClass("btn-primary btn-secondary")
                        $("#extmsg").text("All exts are busy.").toggleClass("text-primary text-danger")
                    }
                })
            }
        },
        // 儲存頁面資料
        _saveLog: function () {
            let g = this

            // 檢查call_log內是否有callid存在，若有才儲存
            if (g.options.call_log.callid) {

                g.options.call_log.memo = g.options.memoEditor.getData()
                console.log('Save log: ', g.options.call_log)

                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: '/saveLog',
                    data: g.options.call_log
                }).done((isSuccess) => {
                    if (isSuccess) {
                        console.log('Post call log to server: OK!')
                    } else {
                        console.log('Post call log to server: Fail!')
                    }
                })
            } else {
                console.log('Nothing to save now (callid is undefined')
            }
        },
        // 呼叫內部_saveLog()儲存
        forceSave: function () {
            let g = this
            g._saveLog()
        },
        _showErrorMsg: function (err) {
            let g = this
            g.options.errormsg.text(err)
            g.options.error.modal('show')
        },
        _uuid: function () {
            let d = Date.now()
            if (typeof performance !== 'undefined' && typeof performance.now === 'function') {
                d += performance.now(); //use high-precision timer if available
            }
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = (d + Math.random() * 16) % 16 | 0;
                d = Math.floor(d / 16);
                return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
            });
        }
    })

})(jQuery)
