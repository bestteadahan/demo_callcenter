$(document).ready(function () {

    // 檢查firstname格式
    function validateName() {
        var fname = $('#fname').val()
        if (fname === "") {
            $("#fname").addClass("is-invalid")
            return false
        } else {
            $("#fname").addClass("is-valid").removeClass("is-invalid")
            return true
        }
    }
    $("#fname").keyup(validateName)

    // 檢查mail帳號格式
    function validateMail() {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var email = $('#account').val()
        if (email != "" && regex.test(email)) {
            $("#account").addClass("is-valid").removeClass("is-invalid")
            return true
        } else {
            $("#account").addClass("is-invalid")
            return false
        }
    }
    $("#account").keyup(validateMail)

    // Create new account
    $("#create").click(function () {
        if (validateMail() && validateName()) {
            $("#confirmcreate").modal("show")
            $("#confirm").click(function () {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: '/accountmanage',
                    data: {
                        action: "createAccount",
                        account: $("#account").val(),
                        fname: $("#fname").val(),
                        lname: $("#lname").val(),
                        role: $("#role").val()
                    }
                }).always(function () {
                    window.location.reload()
                })
            })
        }
    })

    // Delete account
    $(".delete").click(function () {
        opid = $(this).attr("data-user")
    })

    //- 搜尋列偵測keyup事件，開始比對account欄位
    $("#search").keyup(function () {
        var search = $(this).val().toLowerCase()
        //- 將所有tr選取
        var rows = $("#myTable tbody tr").toArray()

        for (index in rows) {
            //- 取得每個tr內第一個td，取文字轉小寫
            let name = rows[index].getElementsByTagName('td')[0].innerHTML.toLowerCase()
            //- 判斷是否有包含特定字詞
            if (name.indexOf(search) > -1) {
                $(rows[index]).css('display', '')
            } else {
                $(rows[index]).css('display', 'none')
            }
        }
    })

    //- 處理畫面文字呈現
    for (let index in users) {
        let user = users[index]
        let selectedRow = $(`#myTable tr[opid="${user.opid}"]`)

        //- 判斷帳號enabled狀態
        if (user.enabled != 1) {
            selectedRow.addClass("table-secondary")
            selectedRow.find(".status option").text('Disabled').attr('value', user.enabled).addClass('text-danger')
        } else {
            selectedRow.find(".status option").text('Enabled').attr('value', user.enabled)
        }

        //- 判斷role
        if (user.type == 2) {
            selectedRow.find(".role option").text('Manager').attr('value', user.type).addClass('text-primary')
        } else {
            selectedRow.find(".role option").text('Operator').attr('value', user.type)
        }
    }

    //- 滑過該row顯示edit按鈕
    $('#myTable tr').on('mouseover', function () {
        let opid = $(this).attr('opid')
        $(`tr[opid="${opid}"] .edit`).removeClass("invisible")
    })

    //- 離開該row隱藏edit按鈕
    $('#myTable tr').on('mouseout', function () {
        let opid = $(this).attr('opid')
        $(`tr[opid="${opid}"] .edit`).addClass("invisible")
    })

    //- 當按下edit後的行為
    $(".edit").click(function () {

        let edit = $(this)
        let selectedOpid = edit.parent().parent().attr('opid')  // tr > td > .edit 到上兩層找opid
        let selectedRow = $(`tr[opid="${selectedOpid}"]`)

        //- 定義確認修改按鈕(class="done")
        let done = $('<i class="done d-inline btn text-primary fa fa-check" data-toggle="modal" data-target="#savechange">')
        //- 定義取消修改按鈕(class="cancel")
        let cancel = $('<i class="cancel d-inline btn text-danger fa fa-times">')

        let deletebtn = selectedRow.find('.delete')

        //- 加入確認&取消按鈕、隱藏編輯按鈕、顯示刪除按鈕
        edit.after(cancel).after(done).hide()
        deletebtn.removeClass('invisible')

        //- 處理Status選單
        let statusSelect = $('<select name="enabled" class="form-control-sm form-control">')
        let statusOption = selectedRow.find('.status option')
        //- 定義新option選項與值
        let newStatusOption, newStatusOptionText, newStatusOptionValue
        if (statusOption.text() == "Enabled") {
            newStatusOptionText = "Disabled"
            newStatusOptionValue = 0
        } else {
            newStatusOptionText = "Enabled"
            newStatusOptionValue = 1
        }
        newStatusOption = $(`<option value=${newStatusOptionValue}>${newStatusOptionText}</option>`)
        //- 將option包至select後加入新option
        statusOption.wrap(statusSelect).after(newStatusOption)

        //- 處理Role選單
        let roleSelect = $('<select name="role" class="form-control-sm form-control">')
        let roleOption = selectedRow.find('.role option')
        //- 定義新option選項與值
        let newRoleOption, newRoleOptionText, newRoleOptionVal
        if (roleOption.text() == "Operator") {
            newRoleOptionText = "Manager"
            newRoleOptionVal = 2
        } else {
            newRoleOptionText = "Operator"
            newRoleOptionVal = 3
        }
        //- 將option包至select後加入新option
        newRoleOption = $(`<option value=${newRoleOptionVal}>${newRoleOptionText}</option>`)
        roleOption.wrap(roleSelect).after(newRoleOption)

        //- 宣告變更的data
        let changeData

        //- 按下確認修改按鈕的動作
        done.click(function () {
            changeData = {
                action: "changeStatus",
                type: selectedRow.find('.role option:selected').val(),
                enabled: selectedRow.find('.status option:selected').val(),
                opid: selectedOpid
            }

            //- 移除select tag、移除沒選擇的選項
            selectedRow.find('.status option:not(:selected)').unwrap().remove()
            selectedRow.find('.role option:not(:selected)').unwrap().remove()

            //- 回復按鈕顯示
            done.remove()
            cancel.remove()
            deletebtn.addClass("invisible")
            edit.toggleClass("invisible").show()
        })

        //- 按下取消修改按鈕的動作
        cancel.click(function () {
            //- 移除select tag、移除"新增加"的選項
            newRoleOption.unwrap().remove()
            newStatusOption.unwrap().remove()

            //- 回復按鈕顯示
            done.remove()
            cancel.remove()
            deletebtn.addClass("invisible")
            edit.toggleClass("invisible").show()
        })

        //- 按下儲存後的動作(in modal)
        $("#save").click(function () {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/accountmanage',
                data: changeData
            }).always(function () {
                window.location.reload()
            })
        })

        // 按下刪除後的動作(in modal)
        $("#delete").click(function () {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/accountmanage',
                data: {
                    action: "deleteAccount",
                    opid: selectedOpid
                }
            }).always(function () {
                window.location.reload()
            })
        })
    })
})