$(document).ready(function () {
    
    // 清除所有client cookie
    localStorage.clear()

    // 驗證密碼
    function validatePw() {
        $("#password").removeClass("is-valid is-invalid")
        var password = $('#password').val()
        if (password === "") {
            $("#password").addClass("is-invalid")
            return false
        } else {
            $("#password").addClass("is-valid")
            return true
        }
    }

    $("#password").keyup(validatePw)

    // 驗證帳號格式
    function validateMail() {
        $("#account").removeClass("is-valid is-invalid")
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var email = $('#account').val()
        if (email != "" && regex.test(email)) {
            $("#account").addClass("is-valid")
            return true
        } else {
            $("#account").addClass("is-invalid")
            return false
        }
    }

    $("#account").keyup(validateMail)

    // 點選login後至DB檢查
    $("#login").click(function () {
        $('#error').removeClass("show")
        if (validateMail() && validatePw()) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/login',
                data: {
                    account: $("#account").val(),
                    password: $("#password").val()
                }
            }).done(function (success) {
                if (success) {
                    location.href = '/'
                } else {
                    $('#error').addClass("show")
                }
            })
        }
    })
})