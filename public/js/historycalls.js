$(document).ready(function () {
    var currentPage = 1  // 目前所在頁面，預設1
    var opfirstname = "all"  // 表單filter篩選op，預設all

    // 如果使用者為operator role，移除表單filter(下拉式選單)
    if (usertype == 3) {
        $("#opfilter").remove()
    }

    // 檢查cookie，如果有page紀錄就複寫目前所在頁面
    if (localStorage.getItem('page') != null) {
        currentPage = parseInt(localStorage.getItem('page'))
    }

    // 檢查cookie，如果有opfilter紀錄就複寫目前顯示的opfirstname，以及選擇option
    if (localStorage.getItem('opfilter') != null) {
        opfirstname = localStorage.getItem('opfilter')
        $("#opfilter option").toArray().forEach(ele => {
            if (ele.value == opfirstname) {
                $(ele).attr('selected', true)
            }
        })
    }

    // Pagination選項
    var paginationOps = {
        totalPages: Math.ceil(totalCounts / countsPerPage),
        visiblePages: 10,
        startPage: currentPage,
        first: 'Latest',
        last: 'Oldest',
        // 當切換頁面時的行為
        onPageClick: function (event, page) {
            // 暫存入cookie
            localStorage.setItem('page', page)
            // 取得該頁資料
            fetchData(page, opfirstname)
        }
    }

    // 建立pagination
    $('#pagenav').twbsPagination(paginationOps)

    // 取得某頁、某op的資料
    function fetchData(page, opfirstname) {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/historycalls',
            data: { currentPage: page, opfirstname: opfirstname }
        }).done(logs => {

            var parent = $("#myTable tbody")

            // 清空現有表單內容
            parent.empty()

            logs.forEach(log => {

                var row = $('<tr>').attr('class', 'clickable-row').attr('id', `${log.callid}`),
                    // endcode = $('<td>').text(`${log.endcode}`),
                    status = $('<td>'),
                    statusimg = $('<img>'),
                    name = $('<td>').text(`${log.name}`),
                    logdt,
                    date = $('<td>'),
                    firstname = $('<td>'),
                    ext = $('<td>').text(`${log.ext}`)

                // console.log(log.endcode)

                // 該通來電狀態圖示
                switch (log.endcode) {
                    case (1):
                        statusimg.attr('src', '/img/misscall.png').attr('title', 'Missed. Timeout/End by caller')
                        break;
                    case (3):
                        statusimg.attr('src', '/img/misscall.png').attr('title', 'Missed. Rejected by op')
                        break;
                    case (4):
                        statusimg.attr('src', '/img/successcall.png').attr('title', 'Catched. End by caller')
                        break;
                    case (5):
                        statusimg.attr('src', '/img/successcall.png').attr('title', 'Catched. End by op')
                        break;
                    case (6):
                        statusimg.attr('src', '/img/others.png').attr('title', 'Others')
                        break;
                }

                status.append(statusimg)

                // 若op帳號已不存在
                if (log.firstname != null) {
                    firstname.text(`${log.firstname}`)
                } else {
                    firstname.text(`(OP removed)`)
                }

                // Calling時間格式
                if (log.callin) {
                    logdt = moment.utc(log.callin).format("YYYY-M-D H:mm:ss")
                    date.text(logdt)
                }

                // 組成row
                row.append(status).append(name).append(date).append(firstname).append(ext)

                // 組成tbody
                parent.append(row)
            })

            // 防止點選播放時跳轉calllog頁面
            let audios = $('audio')
            audios.click(function (e) {
                e.stopPropagation()
                // 暫停其他非正在播放中的element
                audios.toArray().filter(audio => (audio != e.target)).map(audio => audio.pause())
            })

            // 設定當選取某列資料時跳轉頁面
            $('tr.clickable-row').click(function () {
                location.href = '/calllog?callid=' + this.id
            })
        })
    }

    // 取得某op的所有資料筆數
    function getNewTotalCounts(opfirstname) {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/historycalls',
            data: { action: 'getNewTotalCounts', opfirstname: opfirstname }
        }).done(newTotalCounts => {

            // 覆寫totalCounts
            totalCounts = newTotalCounts
            // console.log('from view: ', totalCounts, newTotalCounts)

            // 將現有pagination移除、修改新的option後再建立
            $('#pagenav').twbsPagination('destroy');
            $('#pagenav').twbsPagination($.extend({}, paginationOps, {
                startPage: 1,
                totalPages: Math.ceil(totalCounts / countsPerPage)
            }));
        })
    }

    // 當opfilter選單改變時，重新取得資料總筆數與資料
    $("#opfilter").change(function () {
        opfirstname = $("#opfilter option:selected").val()
        getNewTotalCounts(opfirstname)
        fetchData(currentPage, opfirstname)
        localStorage.setItem('opfilter', opfirstname)
    })
})