$(document).ready(function () {

    // 驗證mail格式
    function validateMail() {
        $("#mail").removeClass("is-valid is-invalid")
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var email = $('#mail').val()
        if (email != "" && regex.test(email)) {
            $("#mail").addClass("is-valid")
            return true
        } else {
            $("#mail").addClass("is-invalid")
            return false
        }
    }

    $("#mail").keyup(validateMail)

    $("#send").click(function () {
        // 如果帳號驗證無誤，post ajax
        if (validateMail()) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/forgetpw',
                data: {
                    mail: $("#mail").val(),
                    host: location.host
                }
            }).always(function () {
                $("#notice").modal({ show: true })
            })
        }
    })

    // 按下關閉後倒數跳轉至登入頁
    $("#close").click(function () {
        $("#msg").prop("hidden", false)

        var t = $("#timer").text()

        // 每隔一秒扣值
        setInterval(count, 1000)
        function count() {
            if (t == 0) {
                location.href = "/login"
            } else {
                $("#timer").text(" " + t + " ")
                t--
            }
        }
    })
})