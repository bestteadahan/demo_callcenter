$(document).ready(function () {

    // 檢查每個.auth function，移除與登入使用者.type不符的element
    $(`.auth:not(.type${type})`).remove()
    $("#hangup").hide()

    // 登入使用者為operator
    if (type != 1) {

        // 檢查推播通知權限
        if (!('Notification' in window)) {
            console.log('This browser does not support notification');
        } else {
            if (Notification.permission != 'granted') {

                Notification.requestPermission()
                    .then(permission => {

                        if (permission === 'default' || permission === 'denied') {
                            alert('Please turn-on notification to catch new coming call from OFW member')
                        }
                        // permission: granted 同意, denied 拒絕, default 未授權
                    })

            } else {
                console.log('User has granted pushing notification!')
            }
        }

        // 判斷目前所在頁面是否為hotline。若不是，則把所有.hotline物件隱藏。若是則建立widget
        if (location.pathname === "/hotline") {
            // 建立 #loop-call widget
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: '/sipRegister',
                data: { action: 'checkStatus' }
            }).done(function (data) {
                if (data.isConnected) {
                    // 若session已為連線狀態，則初始化時帶入分機
                    $('#loop-call').emcall({ trace: true, uri: data.uri, ext: data.ext });
                } else {
                    $('#loop-call').emcall({ trace: true, uri: data.uri });
                }
            })

            // 呼叫checkExt()
            $("#refresh").click(function () {
                $('#loop-call').emcall('checkExt')
            })
        } else {
            $('.hotline').hide()
        }
    }

})