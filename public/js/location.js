(function ($) {
    $.widget("custom.createMap", {
        //設定參數
        options: {
            api: "https://maps.googleapis.com/maps/api/js",
            parameters: { 'key': 'AIzaSyAKWyaTGTuc69mI1C9lAwSHdtLPUTg2GEA' },
            location: null,
            map: null,
            marker: null
        },
        _create: function () {
            let m = this;
            //載入Google Maps API
            $.ajax({
                url: m.options.api,
                data: m.options.parameters,
                dataType: "script",
                cache: true
                // ajax取資料成功(done)後執行
            }).done(function () {
                m.options.map = new google.maps.Map(m.element[0], {
                    zoom: 15,
                });

                if (m.options.location != null) {
                    m.setMap(m.options.location)
                }
            });
        },
        setMap: function (location) {
            let m = this

            // 顯示地圖div
            m.element.removeClass('d-none')
        
            m.options.map.setCenter(location)

            m.options.marker = new google.maps.Marker({
                position: location,
                map: m.options.map
            });
        },
        hide: function () {
            let m = this
            // 隱藏display : none
            m.element.addClass("d-none")

            if (m.options.marker!=null) {
                m.options.marker.setMap(null)
                m.options.marker = null
                m.options.location = null
            }
        }
    });
})(jQuery);